function makeSessionElement(session) {
    const sessionElement = document.createElement("div");

    sessionElement.textContent = JSON.stringify(session);
    //TODO: define buttons

    return sessionElement;
}

class SessionManager {
    constructor() {
        this.sessionStorageName = "DefaultStorageName";

        this.getSessions = () => 
            JSON.parse(window.localStorage.loadItem(sessionStorageName)) || [];

        this.storeSession = (session) => {
            const currentSessions = getSessions();

            currentSessions.push(session);

            window.localStorage.saveItem(sessionStorageName, JSON.stringify(currentSessions));
        }
    }
}

const sessionManager = new SessionManager();

window.onload = () => {
    document.getElementById("SaveButton").addEventListener("click", buttonSave);
    document.getElementById("LoadButton").addEventListener("click", buttonLoad);
    document.getElementById("CloseButton").addEventListener("click", buttonClose);

    const sessionSection = document.getElementById("SessionSesction");

    sessionManager.getSessions().map(makeSessionElement)
        .foreach(sessionElement => sessionSection.appendChild(sessionElement));
};


function onCreated(tab) {
    console.log(`Created new tab: ${tab.id}`)
}

function onError(error) {
    console.log(`Error: ${error}`);
}

function buttonSave() {
    console.log("Inside buttonSave function");
    browser.tabs.query({}, function (tabs) {
        const session = {
            name: "name",
            date: new Date(),
            tabs: tabs.reduce((all, tab) => {
                if(tab.pinned){
                    all.push(tab.url);
                }

                return all;
            }, [])
        }

        sessionManager.storeSession(session);
   });
}

function buttonLoad() {


//
//   console.log("Inside buttonLoad function");
//
//   navigator.clipboard.readText().then(textTemp => {
//       var result = textTemp.split('\n');
//       console.log(result);
//
//       for(const str of result){
//           var creating = browser.tabs.create({
//               url:str,
//               pinned:true
//           });
//           creating.then(onCreated, onError);
//       }
//
//   }).catch(err => {
//       console.error("Does not work!");
//   });
}

function buttonClose() {
    console.log("Close pressed");

    browser.tabs.query({}, function (tabs) {
        for (const tab of tabs){
            if(tab.pinned){
                var removing = browser.tabs.remove(tab.id);
            }
        }

        console.log("Pinned tabs removed");
    });
}

function copyToClipboard(text) {
    function oncopy(event) {
        document.removeEventListener("copy", oncopy, true);
        event.stopImmediatePropagation();
        event.preventDefault();
        event.clipboardData.setData("text/plain", text);
    }
    document.addEventListener("copy", oncopy, true);
    document.execCommand("copy");
}
